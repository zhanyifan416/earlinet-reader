Changelog
=========

Unreleased
----------
Added
~~~~~
- Conversion of HiRElPP files to GEOMS format (ESA/NASA).

0.2.5 - 2018-05-08
------------------
Fixed
~~~~~
- Added Manifest.in to fix pypi installation procedure.

Added
~~~~~
- This changelog.